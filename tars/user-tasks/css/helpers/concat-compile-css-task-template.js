'use strict';

const stringHelper = tars.helpers.stringHelper;

module.exports = function generateTaskContent(browser) {

    browser = browser || '';

    const preprocExtensions = tars.cssPreproc.ext;
    const preprocName = tars.cssPreproc.name;
    const capitalizePreprocName = stringHelper.capitalizeFirstLetter(preprocName);
    const stylesFolderPath = `./markup/${tars.config.fs.staticFolderName}/${preprocName}`;

    let successMessage = `${capitalizePreprocName}-files have been compiled`;
    let errorMessage = 'An error occurred while compiling css';
    let compiledFileName = 'main';
    let generateSourceMaps = false;

    let postProcessors = [];
    let stylesFilesToConcatinate = [];
    let firstStylesFilesToConcatinate = [
        `${stylesFolderPath}/new-normalize.${preprocExtensions}`,
        `${stylesFolderPath}/libraries/**/*.${preprocExtensions}`,
        `${stylesFolderPath}/libraries/**/*.css`,
        `${stylesFolderPath}/mixins.${preprocExtensions}`,
        `${stylesFolderPath}/sprites-${preprocName}/sprite_96.${preprocExtensions}`
    ];
    const generalStylesFilesToConcatinate = [
        `${stylesFolderPath}/fonts.${preprocExtensions}`,
        `${stylesFolderPath}/vars.${preprocExtensions}`,
        `${stylesFolderPath}/GUI.${preprocExtensions}`,
        `${stylesFolderPath}/common.${preprocExtensions}`,
        `${stylesFolderPath}/plugins/**/*.${preprocExtensions}`,
        `${stylesFolderPath}/plugins/**/*.css`,
        `./markup/${tars.config.fs.componentsFolderName}/**/*.${preprocExtensions}`,
        `./markup/${tars.config.fs.componentsFolderName}/**/*.css`
    ];
    const lastStylesFilesToConcatinate = [
        `${stylesFolderPath}/etc/**/*.${preprocExtensions}`,
        `${stylesFolderPath}/etc/**/*.css`,
        `!${stylesFolderPath}/entry/**/*.${preprocExtensions}`,
        `!${stylesFolderPath}/entry/**/*.css`,
        `!./**/_*.${preprocExtensions}`,
        '!./**/_*.css'
    ];

};
