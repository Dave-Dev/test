$(() => {
    $('.js-feedback-slider').slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        nextArrow: '<button class="slider__btn--next slick-arrow slider__btn" aria-label="next" type="button"><svg class="icon service__item-icon" width="40" height="40"><use xlink:href="svg-symbols.svg#arrow"></use></svg></button>',
        prevArrow: '<button class="slider__btn--prev slick-arrow slider__btn" aria-label="prev" type="button"><svg class="icon service__item-icon" width="40" height="40"><use xlink:href="svg-symbols.svg#arrow"></use></svg></button>',
    });
    $('.js-feedback-sert-slider').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        nextArrow: '<button class="slider__btn--next slick-arrow slider__btn" aria-label="next" type="button"><svg class="icon service__item-icon" width="40" height="40"><use xlink:href="svg-symbols.svg#arrow"></use></svg></button>',
        prevArrow: '<button class="slider__btn--prev slick-arrow slider__btn" aria-label="prev" type="button"><svg class="icon service__item-icon" width="40" height="40"><use xlink:href="svg-symbols.svg#arrow"></use></svg></button>',
    });
});
